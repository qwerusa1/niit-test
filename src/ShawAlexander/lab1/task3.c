/*
3. �������� ���������, ������� ��������� �������� ���� �� �����-
��� � �������, �, ��������, � ����������� �� ������� ��� �����.
��������:
45.00D
- �������� �������� � ��������, �
45.00R
- �
��������. ���� ������ �������������� �� �������
%f%c
*/
#include <stdio.h>
void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}
int main()
{
	float value, result;
	char c;
    while (1) {
        printf("Enter the value of an angle in the following format (10.00D/10.00R):\n");
        if (scanf("%f%c", &value, &c) == 2 && (c == 'D' || c == 'R'))
            break;
        else {
            puts("Input error!");
            cleanIn();
        }
    }
	if (c == 'R') {
		result = (180.0 / 3.14159) * value;
		printf("%fD\n", result);
	}
	else if (c == 'D') {
		result = (3.14159 / 180) * value;
		printf("%fR\n", result);
	}
	return 0;
}